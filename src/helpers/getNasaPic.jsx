export const getNasaPic = async () => {
  const url = `https://api.nasa.gov/planetary/apod?api_key=6wOZ0RLdrz6zmvaiCjc7cocKKgn6UjFB8kZ55IpH`;
  const resp = await fetch(url);
  const data = await resp.json();
  const datos = [data];

  const picData = datos.map((img) => ({
    title: img.title,
    copyright: img.copyright,
    date: img.date,
    url: img.url,
  }));
  return picData;
};
