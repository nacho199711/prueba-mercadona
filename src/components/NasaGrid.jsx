import { NasaItem } from "./NasaItem";
import { useFetchNasa } from "../hooks/useFetchNasa";

const NasaGrid = () => {
  const { images, isLoading } = useFetchNasa();

  return (
    <>
      {isLoading && <h2>Cargando...</h2>}

      <div className="card-grid">
        {images.map((image) => (
          <NasaItem key={image.title} {...image} />
        ))}
      </div>
    </>
  );
};
export default NasaGrid;
