import React from "react";
import NasaGrid from "./NasaGrid";
import { useModal } from "../hooks/useModal";
import Modal from "./Modal";
import "./StyleModal.css";

const CrudApp = () => {
  const [isOpenModalCreate, openModalCreate, closeModalCreate] =
    useModal(false);
  const [isOpenModalUpdate, openModalUpdate, closeModalUpdate] =
    useModal(false);

  return (
    <div>
      <h2>Picture of the day</h2>
      <article className="grid-1-2">
        <NasaGrid />
      </article>
      <button onClick={openModalCreate} className="create">
        Create
      </button>

      <Modal
        isOpen={isOpenModalCreate}
        closeModal={closeModalCreate}
        className="modalForm"
      >
        <div>
          <form className="formContainer">
            <h5>Title:</h5>
            <input type="text" name="title" placeholder="Title" />
            <h5>Name:</h5>
            <input type="text" name="Owner" placeholder="Write your name" />
            <h5>Date:</h5>
            <input type="date" name="Date" />
            <input type="submit" value="Send" />
          </form>
        </div>
      </Modal>
      <button onClick={openModalUpdate} className="update">
        Update
      </button>
      <Modal
        isOpen={isOpenModalUpdate}
        closeModal={closeModalUpdate}
        className="modalForm"
      >
        <div>
          <form className="formContainer">
            <h5>Title:</h5>
            <input type="text" name="title" placeholder="Write a title" />
            <h5>Name:</h5>
            <input type="text" name="Owner" placeholder="Write your name" />
            <h5>Date:</h5>
            <input type="date" name="Date" />
            <input type="submit" value="Update" />
          </form>
        </div>
      </Modal>
    </div>
  );
};

export default CrudApp;
