import Modal from "./Modal";
import { useModal } from "../hooks/useModal";
export const NasaItem = ({ title, url, date, copyright }) => {
  const [isOpenModal, openModal, closeModal] = useModal(false);

  return (
    <div className="card">
      <div>
        <button onClick={openModal} className="modalPic">
          <img className="nasaImage" src={url} alt={title} />
        </button>
        <Modal isOpen={isOpenModal} closeModal={closeModal}>
          <img src={url}></img>
        </Modal>
      </div>

      <p>
        <b>Title: </b>
        {title}
      </p>
      <p>
        <b>Date: </b>
        {date}
      </p>
      <p>
        <b>Owner: </b>
        {copyright === undefined ? "This image didn't have Owner" : copyright}
      </p>
    </div>
  );
};
