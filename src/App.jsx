import React from "react";
import CrudApp from "./components/CrudApp";
import "./index.css";

function App() {
  return (
    <div className="allBody">
      <h1>Prueba Mercadona</h1>
      <hr />
      <CrudApp />
    </div>
  );
}

export default App;
